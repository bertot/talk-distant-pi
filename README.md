A talk about the contents of the paper *distant decimals of pi*
==============

In this talk, I try to expose the three algorithms of the paper and give
a hint at the mathematical proofs, but avoid using Coq syntax.

 * Use as basis the recursive, 3 divisions per round, algorithm by Borwein&Borwein
 * Show the relation with the Brent&Salamin algorithm
 * A first (lightweight) part about the BBP algorithm 

Parts of the slides are taken from another talk (under package talk-pi-agm)

