\documentclass[compress]{beamer}
\usepackage[latin1]{inputenc}
\usepackage{color}
\usepackage{hyperref}

\newcommand{\mrightarrow}{\,\buildrel * \over \rightarrow\,}
\mode<presentation>
\setbeamertemplate{footline}[frame number]
%\usepackage{beamerthemesplit}
\title{Distant decimals of \(\pi\)}
\author{Yves Bertot, Laurence Rideau, Laurent Th�ry}
\date{June 2018}
\newcommand{\fst}{\textit{fst}}
\newcommand{\llet}[3]{\ensuremath{\textsf{let}~#1=#2~\textsf{in}~#3}}
\newcommand{\couple}[2]{\ensuremath{\langle#1,#2\rangle}}
\newcommand{\irule}[3]{{\frac{\displaystyle #1}{\displaystyle
#2}{\makebox[1pt][l]{~(#3)}}}}
\definecolor{darkgreen}{rgb}{0,0.5,0}
\newcommand{\green}[1]{\textcolor{darkgreen}{#1}}

\begin{document}
\maketitle
\begin{frame}
\begin{itemize}
\item Arithmetic-geometric means: an algorithm with 3n divisions
\item Arithmetic-geometric means: an algorithm with only one division
\item A few facts about the correctness of individual digits
\item The BBP formula
\end{itemize}
\end{frame}
\begin{frame}
\frametitle{The Arithmetic-Geometric computation}
\begin{itemize}
\item A pair of two sequences of real numbers
\item \(a_0 = 1 \quad b_0 = x\quad a_{n+1} = \frac{a_n + b_n}{2} \quad b_{n+1} = \sqrt{a_n b_n}\)
\item \(\int_{-\infty}^{\infty} \frac{{\rm d} t}{\sqrt{(1 ^2 + t^2)(x ^2 + t^2)}} =
\int_{-\infty}^{\infty} \frac{{\rm d} t}{\sqrt{(a_n^2 + t^2)(b_n^2 + t^2)}}\)
\item \(a_n\) and \(b_n\) converge very fast to the same value (\(a_n\) is larger) 
\item \(a_n - b_n < A^{-2^n}\) 
\item  Number of known digits doubles at every step
\end{itemize}
\end{frame}
\begin{frame}
\frametitle{AGM for \(\pi\)}
\begin{itemize}
\item Define \(f(x)\) as the arithmetic-geometric mean of 1 and \(x\)
\item Key property 
\(\displaystyle \pi = 2\sqrt{2} \frac{f(1/\sqrt{2}) ^
  3}{f'(1/\sqrt{2})}\)
\item Mathematical proofs based on elliptic integrals
\begin{itemize}
\item Improper integrals (Coquelicot)
\item Needed extensions (Chasles with improper integrals)
\item Lots of formulas with squares and square roots\\ slightly beyond Presburger
  automated proofs
\end{itemize}
\end{itemize}
\end{frame}
\begin{frame}
\frametitle{Borwein\&Borwein algorithm}
\begin{itemize}
\item Define 
\(\displaystyle y_n(x) = \frac{a_n(x)}{b_n(x)} \qquad z_n(x) = \frac{b'_n(x)}{a'_n(x)}\)
\item Easy consequence
\(\displaystyle {1 + y_n} = 2 \frac{a_{n+1} b_{n+1}^2}{a_{n} b_{n}^2} \qquad
{1 +  z_n} = 2 \frac{a_{n+1}'}{a'_n}\)
\[y_0(x)=\frac{1}{x}\quad y_{n+1}=\frac{1 + y_n}{2\sqrt{y_n}}\quad
z_1 = \frac{1}{\sqrt{x}} \quad z_{n+1} =
\frac{1 + z_n y_n}{(1 + z_n)\sqrt{y_n}} \]
\item \(\displaystyle \pi_n = (2 + \sqrt{2}) \prod_{i=1}^n \frac{1 + y_i}{1+z_i}\) at \(\displaystyle x = \frac{1}{\sqrt{2}}\)
\end{itemize}
\end{frame}
\begin{frame}
\frametitle{convergence rate}

\[0 \leq \pi_{n+1} - \pi \leq 8 \sqrt{2}\times 531^{-2^n}\]
\end{frame}
\begin{frame}
\frametitle{Brent and Salamin algorithm}
\begin{itemize}
\item Define \(c_n = \frac{1}{2}(a_{n - 1} - b_ {n - 1})\)

\item \(\displaystyle \frac{b_{n+1}'}{b_{n+1}}
- \frac{b_n'}{b_n}
=\frac{b_n}{2 a_n}\left(\frac{a_n}{b_n}\right)'
\qquad
\frac{b_{n+1}'}{b_{n+1}} =
\frac{b_1'}{b_1} -\sqrt{2} \sum_{k = 1}^{n-1} 2 ^ k c_k^2\)

\item \(\displaystyle \pi'_n = \frac{4 a^2_n}{1 - \sum_{k=1}^{n-1} 2 ^ {k - 1} (a_{k - 1} - b_{k - 1})^ 2}\)
\end{itemize}
\end{frame}
\begin{frame}
\frametitle{convergence rate}
\begin{itemize}
\item First approximation \(\displaystyle |\pi'_{n+1} - \pi| \leq 68 \times 531 ^{-2^{n-1}}\)
\item Coarser than Borwein\&Borwein: \(\displaystyle \pi_{n+1} - \pi \leq 8\sqrt{2}\times 531^{-2^{n}}\)
\item Improvement by studying \(|\pi'_{n+2} - \pi'_{n+1}|\)
\[|\pi'_{n+1} - \pi | \leq (132 + 384 \times 2 ^ n) \times 531^{-2^n}\]
\item For computing \(10^6\) decimals of \(\pi\) both \(\pi'_{19}\) and \(\pi_{19}\)
are enough
\begin{itemize}
\item \(132 + 384 \times 2 ^ {19} \leq 2 ^ {28}\)
\end{itemize}
\item Each algorithm computes \(n\) square roots, but \(\pi_n\)
computes \(3n\) divisions, \(\pi'_n\) only half-sums and one full division.
\end{itemize}
\end{frame}
\begin{frame}[fragile]
\frametitle{Rounding errors}
\begin{itemize}
\item Approximating real computations using fixed-point computations
(rounding towards 0)
\item Take the same program code, replace operations
\item How do rounding errors propagate?
\item Amazingly \(|\framebox{\(y_n\)} - y_n |\leq 2 ulp\) and \(|\framebox{\(z_n\)} - z_n |\leq 4 ulp\)
\item \(|\framebox{\({\pi_n}\)} - \pi_n |\leq (21 * n + 2) ulp\)
\item \(|\framebox{\({\pi'_n}\)} - \pi'_n| \leq (160 (\frac{3}{2})^{n+1} + 80 * 3 ^ {n + 1} + 100) ulp\)
\end{itemize}
\end{frame}
\begin{frame}
\frametitle{Total interval for Brent-Salamin}
\begin{itemize}
\item In the end \framebox{\(10 ^ {10 ^ 6 + k} \times \pi'_{19}\)} is within \(2 ^ {40}\) of \(10 ^ {10 ^ 6 + k} \times \pi\)
\item It remains to choose a suitable value of \(k\)
\item It is more efficient to compute \framebox{\(2^{\lfloor
      log_2(10)\rfloor \times 10 ^ 6} \times \pi'_{19}\)}
\item Paper available at \url{https://hal.inria.fr/hal-01582524}
\item Code and instructions
  \url{https://www-sop.inria.fr/marelle/distant-decimals-pi/}
\item Includes a C implementation of the Borwein algorithm, on top of MPFR.

\end{itemize}
\end{frame}
\begin{frame}
\frametitle{The BBP formula}
Work done in Coq by Laurence Rideau and Laurent Th�ry
\[
\pi = \sum_{i=0}^{\infty} \frac{1}{16 ^ i}  \left(\frac{4}{8 i + 1} -
\frac{2}{8 i + 4}  - \frac{1}{8 i + 5} - \frac{1}{8 i + 6}\right).
\]
\begin{itemize}
\item Mathematical justification using Rieman integrals
\item commuting integration and infinite sum: thanks to Coquelicot
\end{itemize}
\end{frame}
\begin{frame}
\frametitle{Computation of a single (hexa-decimal) digit}
\begin{itemize}
\item Computing the \(d^{\rm th}\) hexadecimal digit of \(\pi\)
\item \(\lfloor 16 ^ {d-1} \pi\rfloor (mod 16)\)
\item Choose a precision \(p\)
\item Compute separately each of the sums, taking the modulo right-away
\item Example: \(\sum_{i=0}^\infty \lfloor \frac{2 ^ p 16 ^ {d - 1 - i} \times 4}{8 i + 1} \rfloor (mod 2 ^ p)\)
\end{itemize}
\end{frame}
\begin{frame}
\frametitle{Intuitive example}
\begin{itemize}
\item Transposing in base 10, what is the second digit (on the right of the dot) of
\[\sum_{i=0}^\infty \frac{1}{10 ^ i} \frac{1}{2 i + 1}\]
\item \(1 + 0.03333\ldots + 0.00200\ldots + 0.00014\ldots + 0.00001\ldots + \cdots\)
\end{itemize}
\end{frame}
\begin{frame}
\frametitle{Computation of a single digit (continued)}
\begin{itemize}
\item Only a finite number of terms in the sum, approximately \(d + p/4\) terms
\begin{itemize}
\item No modulo is needed for the last \(p/4\) terms
\end{itemize}
\item Use integer division: uncertainty bounded by 1
\item Accumulated uncertainty is \(d + p/4 + 1\)
\item We need the accumulated uncertainty to be small wrt. \(2 ^ {p - 4}\)
\item Final result is partial: when the value modulo \(2 ^ p\) is closer
to \(2^p\) than the accumulated uncertainty the digit is not known.
\end{itemize}
\end{frame}
\end{document}
